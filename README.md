# Introduction

This project creates a docker image with [angular](https://angular.io/) installed.

The image can be used to build software using angular.

This repository is mirrored to https://gitlab.com/sw4j-net/angular
