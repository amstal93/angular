ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/nodejs:latest

RUN apt-get update && \
    apt-get -y upgrade && \
    npm install -g @angular/cli
